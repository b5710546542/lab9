import java.net.*;

import stopwatch.StopWatch;

/**
 * Main for run WordCounter.
 * @author Jidapar Jettananurak
 *
 */
public class Main {

	public static void main(String[] args) throws MalformedURLException
	{
		StopWatch watch = new StopWatch();
		final String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL( DICT_URL );
		WordCounter counter = new WordCounter();
		watch.start();
		int wordcount = counter.countWords( url );
		int syllables = counter.getSyllablesCount( );
		watch.stop();

		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary");
		System.out.println("Counted " + syllables + " syllables in " + wordcount + " words");
		System.out.printf("Elapsed time: %.3f sec" , watch.getElapsed());
		
	}
	
}
