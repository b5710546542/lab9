import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Scanner;

import stopwatch.StopWatch;
/**
 * Counting syllables in a word by count vowel.
 * @author Jidapar Jettananurak
 *
 */
public class WordCounter {
	
	State state;
	private int syllables , totalSyllables = 0;
	
	public static void main(String[]args) throws IOException{
		
		StopWatch watch = new StopWatch();
		String DICT_URL = "http://se.cpe.ku.ac.th/dictionary.txt";
		URL url = new URL( DICT_URL );
		InputStream input = url.openStream();
		Scanner scanner = new Scanner(input);
		WordCounter counter = new WordCounter();
		int syllables = 0;
		watch.start();
		while(scanner.hasNext()){
			syllables += counter.countSyllables(scanner.nextLine());
		}
		watch.stop();
		System.out.println("Reading words from http://se.cpe.ku.ac.th/dictionary");
		System.out.println("Counted " + syllables + " syllables in " + counter.countWords(url) + " words");
		System.out.printf("Elapsed time: %.3f sec" , watch.getElapsed());
	}
	
	/**
	 * Count syllables from URL.
	 * @param url that want to count
	 * @return number of syllables
	 */
	public int countWords(URL url) {
		
		InputStream in = null;
		try {
			in = url.openStream( );
		} catch (IOException e) {
			
		}
		return countWords(in);
		
	}
	
	/**
	 * Count syllables from inputStream.
	 * @param instream inputStream that want to count
	 * @return number of syllables
	 */
	public int countWords(InputStream instream) {
		int num = 0;
		Scanner input = new Scanner(instream);
		while(input.hasNext())
		{
			num++;
			this.totalSyllables += countSyllables( input.next() );
		}
		return num;
	}
	
	/**
	 * Get total syllables from counting
	 * @return total syllables
	 */
	public int getSyllablesCount(){
		return totalSyllables;
	}
	
	/**
	 * Set state of char of word.
	 * @param newstate
	 */
	public void setState( State newstate ){
		if( newstate != state ) newstate.enterState();
		this.state = newstate;
	}
	
	/**
	 * Count syllables of word.
	 * @param word that want to count 
	 * @return number of syllables
	 */
	public int countSyllables( String word ){
		state = START_STATE;
		syllables = 0;
		word = word.replaceAll("'", "");
		for(int i = 0 ; i < word.length() ; i++){
			state.handlerChar(word.charAt(i));
		}
		
		if(state == DASH_STATE) syllables = 0;
		else if(state == VOWEL_STATE) syllables++;
		else if(state == E_FIRST && syllables == 0) syllables++;
		
		return syllables;
	}

	/**
	 * Check vowel.
	 * @param c character of word
	 * @return true if it is vowel
	 */
	private boolean isVowel( char c ){
		if( "AEIOUaeiou".indexOf(c) >= 0 ) return true;
		return false;
	}

	/**
	 * Check letter.
	 * @param c character of letter
	 * @return true if it is letter
	 */
	private boolean isLetter( char c ){
		return Character.isLetter(c);
	}
	
	/**
	 * Check 'e','E' are first of vowel.
	 * @param c character of word
	 * @return true if it is e of first vowel
	 */
	private boolean isE_First( char c ){
		if( c == 'E' || c == 'e') return true;
		return false;
	}

	/**
	 * Check 'y','Y'.
	 * @param c character of word
	 * @return true if it is 'y','Y'
	 */
	private boolean isY( char c ){
		if( c == 'Y' || c == 'y' ) return true;
		return false;
	}
	
	/**
	 * Check dash.
	 * @param c character of word
	 * @return true if it is dash
	 */
	private boolean isDash( char c ){
		if( c == '-' ) return true;
		return false;
	}
	
	/**
	 * Start state of char of word.
	 */
	State START_STATE = new State(){
		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			if( isDash(c) ) setState(NONWORD);
			else if(isE_First(c)) setState(E_FIRST);
			else if( isVowel(c) ) setState( VOWEL_STATE );
			else if( isLetter(c) ) setState( CONSONANT_STATE );
			else setState( NONWORD );
		}
		
	};
	
	/**
	 * End state of char of word.
	 */
	State END_STATE = new State(){
		
		public void enterState() {
			// Don't do any thing.
			
		}

		public void handlerChar(char c) {
			// Don't do any thing.
		}
		
	};
	
	/**
	 * E state of char of word.
	 */
	State E_FIRST = new State(){

		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			if(isDash(c))setState(DASH_STATE);
			else if(isVowel(c))setState(VOWEL_STATE);
			else if(isLetter(c)){
				syllables++;
				setState(CONSONANT_STATE);
			}
			else setState(NONWORD);
		}
		
	};
	
	/**
	 * Nonword state of char of word.
	 */
	State NONWORD = new State(){
		public void enterState() {
			syllables = 0;
		}

		public void handlerChar(char c) {
			setState(NONWORD);
		}
		
	};
	
	/**
	 * Dash state of char of word.
	 */
	State DASH_STATE = new State(){

		public void enterState() {
			// Don't do any thing.
		}

		public void handlerChar(char c) {
			if( isY(c) ) setState( VOWEL_STATE );
			else if( isE_First(c) ) setState( E_FIRST );
			else if( isVowel(c) ) setState( VOWEL_STATE );
			else if( isLetter(c) ) setState( CONSONANT_STATE ) ;
			else setState( NONWORD );
		}
		
	};
	
	/**
	 * Consonant state of char of word.
	 */
	State CONSONANT_STATE = new State(){
		public void handlerChar( char c ) {
			if( isDash(c) ) setState(DASH_STATE);
			else if( isY(c) ) setState( VOWEL_STATE );
			else if( isE_First(c) ) setState( E_FIRST );
			else if( isVowel(c) ) setState( VOWEL_STATE );
			else if( isLetter(c) ) /*Stay in this state*/ ;	
			else setState( NONWORD );
		}

		public void enterState() {
			// Don't do any thing.
		}

	};
	
	/**
	 * Vowel state of char of word.
	 */
	State VOWEL_STATE = new State(){
		public void handlerChar( char c ){
			if( isDash(c) ){
				syllables++;
				setState(DASH_STATE);
			}
			else if( isY(c) ) {
				syllables++;
				setState(CONSONANT_STATE);
			}
			else if( isVowel(c) ) ;
			else if( isLetter(c) ) {
				syllables++;
				setState( CONSONANT_STATE );
			}
			else setState( NONWORD );
		}
	
		public void enterState(){
			// Don't do any thing.
		}
	};
	
}

/**
 * Status of state.
 * @author Jidapar Jettananurak
 *
 */
interface State{

	public void enterState();
	public void handlerChar( char c );
	
}